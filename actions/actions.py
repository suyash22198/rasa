# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


# This is a simple example for a custom action which utters "Hello World!"

from logging import raiseExceptions
from typing import Any, Text, Dict, List
#
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk import FormValidationAction
from rasa_sdk.types import DomainDict
from rasa_sdk.events import SlotSet
import re
#
#
######akshita 

class ValidateNameForm(FormValidationAction):
    def name(self) -> Text:
        return "validate_applicant_form"

    def validate_emp_name(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,) -> Dict[Text, Any]:
        # If the name is super short, it might be wrong.
        data=tracker.get_slot('emp_name')
        print(f"Applicant name given = {data} length = {len(data)}")
        if len(data) <= 2:
           dispatcher.utter_message(text="That's a very short name. I'm assuming you mis-spelled.")
           return {"emp_name": None}
        else:
           dispatcher.utter_message(text="Nice to hear your name")
           return {"emp_name": data}
           
    def validate_date(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,) -> Dict[Text, Any]:
        # If the name is super short, it might be wrong.
        data=tracker.get_slot('date')        
        pat = re.compile(r"(0[1-9]|[12][0-9]|3[01])[/](0[1-9]|1[012])[/]\d{4}")
        print(f"Applicant name given = {data} length = {len(data)}")
        date_split = data.split("/")
        print(type(date_split[0]))
       
        if re.fullmatch(pat, data):
           if date_split[1] == "02":
            if int(date_split[0])<=29:
                dispatcher.utter_message(text="Great!! Your DOB is saved")
                return {"date": data}
           
            else:
                dispatcher.utter_message(text="Please enter date in correct format DD/MM/YYYY")
                return {"date": None}
           else:
            dispatcher.utter_message(text="Great!! Your DOB is saved")
            return {"date": data}
        else:
          dispatcher.utter_message(text="Please enter date in correct format DD/MM/YYYY")
          return {"date": None}
########akshita close


from haystack.utils import clean_wiki_text, convert_files_to_dicts, fetch_archive_from_http, print_answers
from haystack.nodes import FARMReader, TransformersReader

# Recommended: Start Elasticsearch using Docker via the Haystack utility function
from haystack.utils import launch_es

launch_es()


def start_es():
    import time
    import os
    from subprocess import Popen, PIPE, STDOUT
    es_server = Popen(['elasticsearch-7.9.2/bin/elasticsearch'],
                      stdout=PIPE, stderr=STDOUT,
                      preexec_fn=lambda: os.setuid(1)  # as daemon
                      )
    # wait until ES has started
    time.sleep(30)


start_es()

# Connect to Elasticsearch

from haystack.document_stores import ElasticsearchDocumentStore

document_store = ElasticsearchDocumentStore(host="localhost", username="", password="", index="document")

# Let's first fetch some documents that we want to query
doc_dir = "file"
dicts = convert_files_to_dicts(dir_path=doc_dir, clean_func=clean_wiki_text, split_paragraphs=True)

# Let's have a look at the first 3 entries:
# print(dicts[:3])
document_store.write_documents(dicts)

from haystack.nodes import ElasticsearchRetriever

retriever = ElasticsearchRetriever(document_store=document_store)

reader = FARMReader(model_name_or_path="my_model", use_gpu=True)

from haystack.pipelines import ExtractiveQAPipeline

pipe = ExtractiveQAPipeline(reader, retriever)
# ...or use a util to simplify the output
# Change `minimum` to `medium` or `all` to raise the level of detail
# initialize pipeline
PIPELINE = ExtractiveQAPipeline(reader, retriever)
# initialize API

import tensorflow as tf
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.layers import Embedding, LSTM, Dense
from tensorflow.keras.models import Sequential
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.optimizers import Adam
import pickle
import numpy as np
import os
import string

path = 'questions.txt'
text = open(path).read().lower()
norm_text=open(path).read()
print('corpus length:', len(text))

data = text.split("-  ")
data = ''.join(data)
data = data.split("\n")


class ActionHelloWorld(Action):

    def name(self) -> Text:
        print(Text)
        return "call_haystack"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        data_1=tracker.get_slot('emp_name')
        print("query=",data_1)
        file_name=open("names.txt","r+")
        list_name=[i[:-1].lower() for i in file_name]
        inp= str(data_1).strip().lower()
        but_l=[]
        print(but_l)
        for i in list_name:
            if i.find(inp)!=-1:
                but_l.append({"title": i.title(),"payload": i.title()})
        if len(but_l)>1:
            button_list=but_l
            but_l=[] 
            dispatcher.utter_message(text="Choose one",buttons=button_list)
        else:
            q = str(tracker.latest_message['text'])
            print(q)
            pred = PIPELINE.run(query=q, params={"Retriever": {"top_k": 3}, "Reader": {"top_k": 1}})
            try:
                print_answers(pred, details="all")
                print(str(pred['answers'][0]).split("',")[1].split(',')[0][7:])
                score=float(str(pred['answers'][0]).split("',")[1].split(',')[0][7:].strip())
                if score < 0.1:
                    raise Exception("I don't understand")
                answer = str(pred['answers'][0]).split("',")[0][17:]
            except:
                answer = "Sorry No result Found"

            prev = answer
            prev = ' ' + prev

            def clean_text(text):
                tokens = text.split()
                table = str.maketrans('', '', string.punctuation)
                tokens = [w.translate(table) for w in tokens]
                tokens = [word for word in tokens if word.isalpha()]
                tokens = [word.lower() for word in tokens]
                return tokens
            print("Before Cleaning",prev)
            prev = clean_text(prev)
            print("After cleaning",prev)
            import spacy
            nlp = spacy.load("en_core_web_sm")
            spacy_stopwords = spacy.lang.en.stop_words.STOP_WORDS
            # len(spacy_stopwords)
            # for stop_word in list(spacy_stopwords)[:10]:
            #  print(stop_word)
            try:
                prev = ' '.join(prev)
                nltk.download("stopwords")
                nltk.download('punkt')
                stop_words = set(stopwords.words('english'))

                word_tokens = word_tokenize(prev)

                # filtered_sentence = [w for w in word_tokens if not w.lower() in stop_words]

                filtered_sentence = []

                for w in word_tokens:
                    if w not in stop_words:
                        filtered_sentence.append(w)

                filtered_sentence = set(filtered_sentence)
                print("Filtered sentence",filtered_sentence)
                predicted = []
                l = []
                for i in filtered_sentence:
                    l = list(filter(lambda k: i in k, data))
                    if len(l)!=0:
                        predicted.append(l)
                flatten_list = [j for sub in predicted for j in sub]
                print("Flatten list",flatten_list)
                # print("Most Probable Sentences")
                # for i in flatten_list:
                # print(i)
                # print("\nTotal Suggestions : ",len(flatten_list))
                print("predicted",predicted)
                recommended = [i[0] for i in predicted]
                message = []
                for i in set(recommended):
                    start=text.find(i)
                    end=start+text[start:].find("\n")
                    print(start,end,norm_text[start:end])
                    message.append({"title": norm_text[start:end],"payload":norm_text[start:end]})
                    print(message)
            except:
                message=[]
            dispatcher.utter_message(text=answer, buttons=message)
        return []
class Actionapplicant(Action):

    def name(self) -> Text:
        return "action_applicant"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:        
       
        data2=tracker.get_slot('date')
        data3=tracker.get_slot('gradyear')
        data4=tracker.get_slot('exp_year')
        data1=tracker.get_slot('emp_name')
        data5=tracker.get_slot('skill')
        dispatcher.utter_message(text=f"The details are as follows: 1. Your good name is {data1} 2. Your DOB is {data2} 3.Your Graduation year is {data3} 4. Your year of experience is {data4} 5.Your skills are {data5}")

        return []
