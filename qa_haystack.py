from haystack.utils import clean_wiki_text, convert_files_to_dicts, fetch_archive_from_http, print_answers
from haystack.nodes import FARMReader, TransformersReader

# Recommended: Start Elasticsearch using Docker via the Haystack utility function
from haystack.utils import launch_es
launch_es()


def start_es():
  import time
  import os
  from subprocess import Popen, PIPE, STDOUT
  es_server = Popen(['elasticsearch-7.9.2/bin/elasticsearch'],
                   stdout=PIPE, stderr=STDOUT,
                   preexec_fn=lambda: os.setuid(1)  # as daemon
                  )
  #wait until ES has started
  time.sleep(30)
start_es()

# Connect to Elasticsearch

from haystack.document_stores import ElasticsearchDocumentStore
document_store = ElasticsearchDocumentStore(host="localhost", username="", password="", index="document")


# Let's first fetch some documents that we want to query
doc_dir = "file"
dicts = convert_files_to_dicts(dir_path=doc_dir, clean_func=clean_wiki_text, split_paragraphs=True)

# Let's have a look at the first 3 entries:
# print(dicts[:3])
document_store.write_documents(dicts)

from haystack.nodes import ElasticsearchRetriever
retriever = ElasticsearchRetriever(document_store=document_store)

reader = FARMReader(model_name_or_path="deepset/roberta-base-squad2", use_gpu=True)

from haystack.pipelines import ExtractiveQAPipeline
pipe = ExtractiveQAPipeline(reader, retriever)
# ...or use a util to simplify the output
# Change `minimum` to `medium` or `all` to raise the level of detail
while 1:
  # start_es()
  question=input().strip()
  prediction = pipe.run(query=question, params={"Retriever": {"top_k": 3}, "Reader": {"top_k": 1}})
  print_answers(prediction, details="minimum")
